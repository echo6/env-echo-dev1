module "this" {
  source        = "git::https://gitlab.com/echo6/terraform-aws-echo.git?ref=f7f1ea376c6911d36aeea4acabecbe5d1f0a35db"
  bucket_prefix = var.git
  public        = module.config.config.public
  protection    = module.config.config.protection
  key           = "blah123"
}

module "config" {
  source = "git::https://gitlab.com/echo6/terraform-config-dev.git?ref=main"
}

terraform {
  backend "s3" {}
}

variable "git" {}
